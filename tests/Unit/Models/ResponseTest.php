<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/24/18
 * Time: 9:53 AM
 */

namespace Tests\Smorken\Auth\Proxy\Common\Unit\Models;

use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException;
use Smorken\Auth\Proxy\Common\Exceptions\InvalidException;
use Smorken\Auth\Proxy\Common\Exceptions\SystemException;
use Smorken\Auth\Proxy\Common\Models\Response;
use Smorken\Auth\Proxy\Common\Models\User;

class ResponseTest extends TestCase
{

    public function testFromExceptionAuthenticationException(): void
    {
        $sut = new Response();
        $r = $sut->fromException(new AuthenticationException('Hidden.'));
        $this->assertStringContainsString('There was an error authenticating', $r->message);
        $this->assertEquals(401, $r->getStatus());
    }

    public function testFromExceptionInvalidException(): void
    {
        $sut = new Response();
        $r = $sut->fromException(new InvalidException("Oops, wrong!"));
        $this->assertStringContainsString('Invalid username and/or password', $r->message);
        $this->assertEquals(401, $r->getStatus());
    }

    public function testFromExceptionNotDisplayable(): void
    {
        $sut = new Response();
        $r = $sut->fromException(new \Exception("Foo Bar"));
        $this->assertFalse($r->authenticated);
        $this->assertTrue($r->error);
        $this->assertEquals(500, $r->getStatus());
        $this->assertStringContainsString('There was an error connecting', $r->message);
    }

    public function testFromExceptionSystemException(): void
    {
        $sut = new Response();
        $r = $sut->fromException(new SystemException('Hidden.'));
        $this->assertStringContainsString('There was an unhandled error', $r->message);
        $this->assertEquals(500, $r->getStatus());
    }

    public function testFromUserAuthenticated(): void
    {
        $sut = new Response();
        $r = $sut->fromUser(new User(['id' => '1234', 'username' => 'sam']), true);
        $expected = [
            'status'        => 200,
            'error'         => false,
            'message'       => null,
            'authenticated' => true,
            'user'          => [
                'id'         => '1234',
                'username'   => 'sam',
                'first_name' => null,
                'last_name'  => null,
                'email'      => null,
                'data'       => [],
            ],
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testFromUserAuthenticatedIncompleteData(): void
    {
        $sut = new Response();
        $r = $sut->fromUser(new User(), true);
        $expected = [
            'status'        => 401,
            'error'         => true,
            'message'       => 'Incomplete user data.',
            'authenticated' => false,
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testFromUsers(): void
    {
        $sut = new Response();
        $coll = [new User(['id' => '1234', 'username' => 'sam']), new User(['id' => '12345', 'username' => 'sam2'])];
        $r = $sut->fromUsers($coll);
        $expected = [
            'status'        => 200,
            'error'         => false,
            'message'       => null,
            'authenticated' => false,
            'users'         => [
                [
                    'id'         => '1234',
                    'username'   => 'sam',
                    'first_name' => null,
                    'last_name'  => null,
                    'email'      => null,
                    'data'       => [],
                ],
                [
                    'id'         => '12345',
                    'username'   => 'sam2',
                    'first_name' => null,
                    'last_name'  => null,
                    'email'      => null,
                    'data'       => [],
                ],
            ],
        ];
        $this->assertEquals($expected, $r->toArray());
    }

    public function testIsAuthenticatedFalseWhenError(): void
    {
        $sut = new Response(
            [
                'status'        => 400,
                'error'         => true,
                'message'       => null,
                'authenticated' => true,
                'user'          => [
                    'id'         => '1234',
                    'username'   => 'sam',
                    'first_name' => null,
                    'last_name'  => null,
                    'email'      => null,
                    'data'       => [],
                ],
            ]
        );
        $this->assertFalse($sut->isAuthenticated());
    }

    public function testIsAuthenticatedFalseWhenInvalidUser(): void
    {
        $sut = new Response(
            [
                'status'        => 200,
                'error'         => false,
                'message'       => null,
                'authenticated' => true,
                'user'          => [
                    'id'         => '1234',
                    'username'   => null,
                    'first_name' => null,
                    'last_name'  => null,
                    'email'      => null,
                    'data'       => [],
                ],
            ]
        );
        $this->assertFalse($sut->isAuthenticated());
    }

    public function testIsAuthenticatedFalseWhenNoUser(): void
    {
        $sut = new Response(
            [
                'status'        => 200,
                'error'         => false,
                'message'       => null,
                'authenticated' => true,
            ]
        );
        $this->assertFalse($sut->isAuthenticated());
    }

    public function testIsAuthenticatedTrue(): void
    {
        $sut = new Response(
            [
                'status'        => 200,
                'error'         => false,
                'message'       => null,
                'authenticated' => true,
                'user'          => [
                    'id'         => '1234',
                    'username'   => 'sam',
                    'first_name' => null,
                    'last_name'  => null,
                    'email'      => null,
                    'data'       => [],
                ],
            ]
        );
        $this->assertTrue($sut->isAuthenticated());
    }

    public function testRehydrate(): void
    {
        $sut = new Response();
        $expected = [
            'status'        => 401,
            'error'         => true,
            'message'       => 'Some error.',
            'authenticated' => false,
        ];
        $r = $sut->rehydrate($expected);

        $this->assertEquals($expected, $r->toArray());
    }

    public function testRehydrateWithUser(): void
    {
        $sut = new Response();
        $expected = [
            'status'        => 200,
            'error'         => false,
            'message'       => null,
            'authenticated' => true,
            'user'          => [
                'id'         => '1234',
                'username'   => 'sam',
                'first_name' => null,
                'last_name'  => null,
                'email'      => null,
                'data'       => [],
            ],
        ];
        $r = $sut->rehydrate($expected);

        $this->assertEquals($expected, $r->toArray());
    }

    public function testRehydrateWithUsers(): void
    {
        $sut = new Response();
        $expected = [
            'status'        => 200,
            'error'         => false,
            'message'       => null,
            'authenticated' => false,
            'users'         => [
                [
                    'id'         => '1234',
                    'username'   => 'sam',
                    'first_name' => null,
                    'last_name'  => null,
                    'email'      => null,
                    'data'       => [],
                ],
                [
                    'id'         => '12345',
                    'username'   => 'sam2',
                    'first_name' => null,
                    'last_name'  => null,
                    'email'      => null,
                    'data'       => [],
                ],
            ],
        ];
        $r = $sut->rehydrate($expected);
        $this->assertEquals($expected, $r->toArray());
    }

    public function testToArrayDefaults(): void
    {
        $sut = new Response();
        $r = $sut->toArray();
        $expected = [
            'status'        => null,
            'error'         => false,
            'authenticated' => false,
            'message'       => null,
        ];
        $this->assertEquals($expected, $r);
    }

    public function testToArrayWithUserDefaults(): void
    {
        $sut = new Response(['user' => new User()]);
        $r = $sut->toArray();
        $expected = [
            'status'        => null,
            'error'         => false,
            'message'       => null,
            'authenticated' => false,
            'user'          => [
                'id'         => null,
                'username'   => null,
                'first_name' => null,
                'last_name'  => null,
                'email'      => null,
                'data'       => [],
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testToJsonDefaults(): void
    {
        $sut = new Response();
        $r = $sut->toJson();
        $expected = '{"status":null,"error":false,"authenticated":false,"message":null}';
        $this->assertEquals($expected, $r);
    }

    public function testToJsonWithUserDefaults(): void
    {
        $sut = new Response(['user' => new User()]);
        $r = $sut->toJson();
        $expected = '{"status":null,"error":false,"authenticated":false,"message":null,"user":{"id":null,"username":null,"first_name":null,"last_name":null,"email":null,"data":[]}}';
        $this->assertEquals($expected, $r);
    }
}
