<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/23/18
 * Time: 10:04 AM
 */

namespace Tests\Smorken\Auth\Proxy\Common\Unit\Providers;

use GuzzleHttp\Client;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Smorken\Auth\Proxy\Common\Providers\Guzzle;

class GuzzleTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function test500ErrorCanRetryAndFail(): void
    {
        [$sut, $client, $logger] = $this->getSut();
        $response = m::mock(ResponseInterface::class);
        $client->shouldReceive('request')
               ->twice()
               ->with('POST', 'http://localhost/authenticate', m::type('array'))
               ->andReturn($response);
        $response->shouldReceive('getBody->getContents')
                 ->twice()
                 ->andReturn($this->getUser(['status' => 500, 'authenticated' => false]));
        $r = $sut->authenticate('foo', 'bar');
        $this->assertFalse($r->isAuthenticated());
        $this->assertTrue($r->isError());
    }

    public function test500ErrorCanRetryAndSucceed(): void
    {
        [$sut, $client, $logger] = $this->getSut();
        $response = m::mock(ResponseInterface::class);
        $client->shouldReceive('request')
               ->twice()
               ->with('POST', 'http://localhost/authenticate', m::type('array'))
               ->andReturn($response);
        $response->shouldReceive('getBody->getContents')
                 ->twice()
                 ->andReturn($this->getUser(['status' => 500, 'authenticated' => false]), $this->getUser());
        $r = $sut->authenticate('foo', 'bar');
        $this->assertTrue($r->isAuthenticated());
        $this->assertFalse($r->isError());
    }

    public function testAuthenticateErrorDoesntRetry(): void
    {
        [$sut, $client, $logger] = $this->getSut();
        $response = m::mock(ResponseInterface::class);
        $client->shouldReceive('request')
               ->once()
               ->with('POST', 'http://localhost/authenticate', m::type('array'))
               ->andReturn($response);
        $response->shouldReceive('getBody->getContents')
                 ->once()
                 ->andReturn($this->getUser(['status' => 401, 'authenticated' => false]));
        $r = $sut->authenticate('foo', 'bar');
        $this->assertFalse($r->isAuthenticated());
        $this->assertTrue($r->isError());
    }

    public function testAuthenticateOk(): void
    {
        [$sut, $client, $logger] = $this->getSut();
        $response = m::mock(ResponseInterface::class);
        $client->shouldReceive('request')
               ->once()
               ->with('POST', 'http://localhost/authenticate', m::type('array'))
               ->andReturn($response);
        $response->shouldReceive('getBody->getContents')
                 ->once()
                 ->andReturn($this->getUser());
        $r = $sut->authenticate('foo', 'bar');
        $this->assertTrue($r->isAuthenticated());
    }

    public function testRequestCanHandleException(): void
    {
        [$sut, $client, $logger] = $this->getSut();
        $e = new \Exception('Test');
        $client->shouldReceive('request')
               ->twice()
               ->with('POST', 'http://localhost/authenticate', m::type('array'))
               ->andThrow($e);
        $logger->shouldReceive('error')
               ->twice()
               ->with($e);
        $r = $sut->authenticate('foo', 'bar');
        $this->assertFalse($r->isAuthenticated());
        $this->assertTrue($r->isError());
        $this->assertEquals('There was an error connecting to the authentication provider.', $r->message);
    }

    public function testSearchOk(): void
    {
        [$sut, $client, $logger] = $this->getSut();
        $response = m::mock(ResponseInterface::class);
        $client->shouldReceive('request')
               ->once()
               ->with('POST', 'http://localhost/search', m::type('array'))
               ->andReturn($response);
        $response->shouldReceive('getBody->getContents')
                 ->once()
                 ->andReturn($this->getUsers());
        $r = $sut->search(['id' => '1']);
        $this->assertFalse($r->isAuthenticated());
        $this->assertTrue($r->hasUsers());
        $this->assertCount(1, $r->users);
    }

    protected function getSut(): array
    {
        $logger = m::mock(\Psr\Log\LoggerInterface::class);
        $client = m::mock(Client::class);
        $sut = new Guzzle($client, []);
        $sut->setLog($logger);
        return [$sut, $client, $logger];
    }

    protected function getUser(array $overrides = []): string
    {
        $user = [
            'status' => 200,
            'error' => false,
            'authenticated' => true,
            'message' => null,
            'user' => [
                'id' => 1,
                'username' => 'foo',
                'first_name' => 'Joe',
                'last_name' => 'Bobber',
                'email' => 'joe@foo.com',
                'data' => [],
            ],
        ];
        $user = array_replace_recursive($user, $overrides);
        return json_encode($user);
    }

    protected function getUsers(array $overrides = []): string
    {
        $data = [
            'status' => 200,
            'error' => false,
            'authenticated' => false,
            'users' => [
                [
                    'id' => 1,
                    'username' => 'foo',
                    'first_name' => 'Joe',
                    'last_name' => 'Bobber',
                    'email' => 'joe@foo.com',
                    'data' => [],
                ],
            ],
        ];
        $data = array_replace_recursive($data, $overrides);
        return json_encode($data);
    }
}
