<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/26/18
 * Time: 6:52 AM
 */

namespace Smorken\Auth\Proxy\Common\Traits;

use Illuminate\Contracts\Foundation\Application;
use Psr\Log\LoggerInterface;
use Smorken\Auth\Proxy\Common\Loggers\NullLogger;

trait Log
{

    protected bool $debug = false;

    /**
     * @var LoggerInterface|null
     */
    protected ?LoggerInterface $logger = null;

    /**
     * @param  mixed  $msg
     * @param  array  $context
     */
    public function debug(mixed $msg, array $context = []): void
    {
        if ($this->debug) {
            $l = $this->getLog();
            $l->debug($msg, $this->cleanContext($context));
        }
    }

    /**
     * @param  \Psr\Log\LoggerInterface  $logger
     */
    public function setLog(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    protected function cleanContext(array $context): array
    {
        $cleaned = [];
        foreach ($context as $k => $v) {
            if (stristr($k, 'pass')) {
                $v = '[ REDACTED ]';
            }
            $cleaned[$k] = $v;
        }
        return $cleaned;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    protected function getLog(): LoggerInterface
    {
        if (!$this->logger && function_exists('app')) {
            if (app() instanceof Application) {
                if (class_exists(\Illuminate\Contracts\Logging\Log::class)) {
                    $this->logger = app(\Illuminate\Contracts\Logging\Log::class);
                } else {
                    $this->logger = app(\Psr\Log\LoggerInterface::class);
                }
            } elseif (app() instanceof \Smorken\Application\App) {
                $this->logger = app('logger.errors');
            }
        }
        if (!$this->logger) {
            $this->logger = new NullLogger();
        }
        return $this->logger;
    }
}
