<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/14/18
 * Time: 7:28 AM
 */

namespace Smorken\Auth\Proxy\Common\Loggers;

use Smorken\Auth\Proxy\Common\Contracts\Logger;

class NullLogger implements Logger
{

    public function __call($name, $arguments)
    {
        return null;
    }

    public function alert($message, array $context = [])
    {
    }

    public function critical($message, array $context = [])
    {
    }

    public function debug($message, array $context = [])
    {
    }

    public function emergency($message, array $context = [])
    {
    }

    public function error($message, array $context = [])
    {
    }

    public function info($message, array $context = [])
    {
    }

    public function log($level, $message, array $context = [])
    {
    }

    public function notice($message, array $context = [])
    {
    }

    public function warning($message, array $context = [])
    {
    }
}
