<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 12:19 PM
 */

namespace Smorken\Auth\Proxy\Common\Contracts\Models;

/**
 * Interface User
 *
 * @package Smorken\Auth\Proxy\Common\Contracts\Models
 *
 * @property string $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property mixed $data
 */
interface User extends Model
{

    /**
     * @return bool
     */
    public function validate(): bool;
}
