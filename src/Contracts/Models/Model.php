<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/25/18
 * Time: 2:40 PM
 */

namespace Smorken\Auth\Proxy\Common\Contracts\Models;

interface Model
{

    public function getAttribute(string $key): mixed;

    /**
     * @param  array  $attributes
     * @return $this
     */
    public function newInstance(array $attributes = []): static;

    /**
     * @param  array  $data
     * @return $this
     */
    public function rehydrate(array $data): self;

    public function setAttribute(string $key, mixed $value): void;

    public function toArray(): array;

    public function toJson(): string;
}
