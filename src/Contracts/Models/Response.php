<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 1:48 PM
 */

namespace Smorken\Auth\Proxy\Common\Contracts\Models;

/**
 * Interface Response
 *
 * @package Smorken\Auth\Proxy\Common\Contracts\Models
 *
 * @property int $status
 * @property bool $error
 * @property bool $authenticated
 * @property string $message
 * @property \Smorken\Auth\Proxy\Common\Contracts\Models\User|null $user
 * @property iterable|null $users
 */
interface Response extends Model
{

    /**
     * @param  \Throwable  $e
     * @return $this
     */
    public function fromException(\Throwable $e): static;

    /**
     * @param  \Smorken\Auth\Proxy\Common\Contracts\Models\User  $user
     * @param  bool  $authenticated
     * @return $this
     */
    public function fromUser(User $user, bool $authenticated = false): static;

    /**
     * @param  iterable  $users
     * @return $this
     */
    public function fromUsers(iterable $users): static;

    /**
     * @return int
     */
    public function getStatus(): int;

    /**
     * @return bool
     */
    public function hasUser(): bool;

    /**
     * @return bool
     */
    public function hasUsers(): bool;

    /**
     * @return bool
     */
    public function isAuthenticated(): bool;

    /**
     * @return bool
     */
    public function isError(): bool;
}
