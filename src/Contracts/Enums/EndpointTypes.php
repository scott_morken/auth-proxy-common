<?php

namespace Smorken\Auth\Proxy\Common\Contracts\Enums;

interface EndpointTypes
{

    const AUTHENTICATE = 'authenticate';
    const SEARCH = 'search';
}
