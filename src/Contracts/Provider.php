<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/24/18
 * Time: 2:56 PM
 */

namespace Smorken\Auth\Proxy\Common\Contracts;

use Smorken\Auth\Proxy\Common\Contracts\Enums\EndpointTypes;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Models\User;

interface Provider
{

    /**
     * @param  string  $username
     * @param  string  $password
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function authenticate(string $username, string $password): Response;

    /**
     * @param  string|null  $str
     * @return string|null
     */
    public function encode(?string $str): ?string;

    /**
     * @return mixed
     */
    public function getBackend(): mixed;

    /**
     * @param  string  $key
     * @param  null  $default
     * @return mixed
     */
    public function getConfigItem(string $key, mixed $default = null): mixed;

    /**
     * @param  string  $type
     * @return string|null
     */
    public function getEndpoint(string $type = EndpointTypes::AUTHENTICATE): ?string;

    /**
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function getResponseModel(): Response;

    /**
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\User
     */
    public function getUserModel(): User;

    /**
     * @param  array  $criteria
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function search(array $criteria): Response;
}
