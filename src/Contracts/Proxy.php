<?php

namespace Smorken\Auth\Proxy\Common\Contracts;

use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Models\User;

interface Proxy
{
    public function addData(bool $should_add): void;

    /**
     * @param  string  $username
     * @param  string  $password
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function authenticate(string $username, string $password): Response;

    /**
     * @param  mixed  $proxy_data
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\User
     */
    public function convertToUser(mixed $proxy_data): User;

    /**
     * @return mixed
     */
    public function getBackend(): mixed;

    /**
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function getResponseModel(): Response;

    /**
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\User
     */
    public function getUserModel(): User;

    /**
     * @param  array  $criteria
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function search(array $criteria): Response;

    /**
     * @param $backend
     * @return void
     */
    public function setBackend($backend): void;
}
