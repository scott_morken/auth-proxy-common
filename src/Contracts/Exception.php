<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 2:32 PM
 */

namespace Smorken\Auth\Proxy\Common\Contracts;

interface Exception
{

    /**
     * @return string
     */
    public function display(): string;
}
