<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 2:33 PM
 */

namespace Smorken\Auth\Proxy\Common\Exceptions;

use Smorken\Auth\Proxy\Common\Contracts\Exception;

abstract class Base extends \Exception implements Exception
{

    protected string $default_msg = 'There was an error authenticating.';

    protected ?string $display = null;

    public function __construct($message, $display = null, $code = 0, \Exception $previous = null)
    {
        $this->display = $display;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function display(): string
    {
        if ($this->display) {
            return $this->display;
        }
        return $this->default_msg;
    }
}
