<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 2:34 PM
 */

namespace Smorken\Auth\Proxy\Common\Exceptions;

class InvalidException extends AuthenticationException
{

    protected string $default_msg = 'Invalid username and/or password.';
}
