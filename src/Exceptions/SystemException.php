<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 2:41 PM
 */

namespace Smorken\Auth\Proxy\Common\Exceptions;

class SystemException extends Base
{

    protected string $default_msg = 'There was an unhandled error.  Please try your request again later.';

    public function __construct($message, $display = null, $code = 500, \Exception $previous = null)
    {
        parent::__construct($message, $display, $code, $previous);
    }
}
