<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/24/18
 * Time: 1:22 PM
 */

namespace Smorken\Auth\Proxy\Common\Exceptions;

class ThrottleException extends AuthenticationException
{

    protected string $default_msg = 'Too many authentication attempts. Please try again in few minutes.';

    public function __construct($message, $display = null, $code = 423, \Exception $previous = null)
    {
        parent::__construct($message, $display, $code, $previous);
    }
}
