<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 2:40 PM
 */

namespace Smorken\Auth\Proxy\Common\Exceptions;

class AuthenticationException extends Base
{

    public function __construct($message, $display = null, $code = 401, \Exception $previous = null)
    {
        parent::__construct($message, $display, $code, $previous);
    }
}
