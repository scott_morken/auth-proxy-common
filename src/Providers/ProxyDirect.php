<?php

namespace Smorken\Auth\Proxy\Common\Providers;

use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Contracts\Proxy;
use Smorken\Auth\Proxy\Common\Models\Response;

class ProxyDirect extends Base implements Provider
{

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Proxy
     */
    protected mixed $backend;

    public function __construct(Proxy $proxy, array $config)
    {
        $this->backend = $proxy;
        parent::__construct($config);
    }

    /**
     * @param  string  $username
     * @param  string  $password
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function authenticate(string $username, string $password): Response
    {
        try {
            return $this->getBackend()
                        ->authenticate($username, $password);
        } catch (\Exception $e) {
            $this->getLogger()
                 ->error($e);
            return $this->getResponseModel()
                        ->fromException($e);
        }
    }

    /**
     * @param  array  $criteria
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function search(array $criteria): Response
    {
        try {
            return $this->getBackend()
                        ->search($criteria);
        } catch (\Exception $e) {
            $this->getLogger()
                 ->error($e);
            return $this->getResponseModel()
                        ->fromException($e);
        }
    }
}
