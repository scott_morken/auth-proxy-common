<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/24/18
 * Time: 2:57 PM
 */

namespace Smorken\Auth\Proxy\Common\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Smorken\Auth\Proxy\Common\Contracts\Enums\EndpointTypes;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Exceptions\AuthenticationException;
use Smorken\Auth\Proxy\Common\Exceptions\SystemException;

class Guzzle extends Base
{

    /**
     * @var Client
     */
    protected mixed $backend;

    protected array $config = [];

    protected array $default_config = [
        'endpoints' => [
            EndpointTypes::AUTHENTICATE => 'http://localhost/authenticate',
            EndpointTypes::SEARCH => 'http://localhost/search',
        ],
        'token' => null,
        'host' => null,
        'allowed_retries' => 1,
        'backend_options' => [
            'connect_timeout' => 5,
        ],
    ];

    protected int $retries = 0;

    public function __construct(Client $client, array $config)
    {
        $this->backend = $client;
        parent::__construct($config);
    }

    public function authenticate(string $username, string $password): Response
    {
        $config = $this->getConfigItem('backend_options', []);
        $config[RequestOptions::FORM_PARAMS] = $this->getFormParams(
            [
                'username' => $username,
                'password' => $password,
                '_host' => $this->getConfigItem('host'),
                '_data' => $this->getConfigItem('data', 0),
            ]
        );
        $config[RequestOptions::HEADERS] = array_replace($config['headers'] ?? [], $this->getHeaders());
        return $this->handleRetries($this->doRequest($this->getEndpoint(), $config), $username, $password);
    }

    /**
     * @param  array  $criteria
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function search(array $criteria): Response
    {
        $config = $this->getConfigItem('backend_options', []);
        $criteria['_host'] = $this->getConfigItem('host');
        $criteria['_data'] = $this->getConfigItem('data', 0);
        $config[RequestOptions::FORM_PARAMS] = $this->getFormParams(
            $criteria
        );
        $config[RequestOptions::HEADERS] = array_replace($config['headers'] ?? [], $this->getHeaders());
        return $this->doRequest($this->getEndpoint(EndpointTypes::SEARCH), $config);
    }

    protected function convertPsr7ResponseToResponse(ResponseInterface $response): Response
    {
        $body = $response->getBody()
                         ->getContents();
        $arr = json_decode($body, true);
        if (is_array($arr)) {
            $r = $this->getResponseModel()
                      ->rehydrate($arr);
        } else {
            $r = $this->unhandledResponseToException($response, $body);
        }
        return $r;
    }

    protected function doRequest(string $endpoint, array $config): Response
    {
        try {
            $msg = $this->getBackend()
                        ->request('POST', $endpoint, $config);
            return $this->convertPsr7ResponseToResponse($msg);
        } catch (\Exception $e) {
            $this->getLogger()
                 ->error($e);
            return $this->getResponseModel()
                        ->fromException($e);
        }
    }

    protected function getFormParams(array $params): array
    {
        $encoded = [];
        foreach ($params as $k => $v) {
            $encoded[$k] = $this->encode($v);
        }
        return $encoded;
    }

    protected function getHeaders(): array
    {
        return [
            'X-Auth-Proxy-Token' => $this->getConfigItem('token'),
            'Accept' => 'application/json',
        ];
    }

    protected function handleRetries(Response $response, string $username, string $password): Response
    {
        $allowed_retries = $this->getConfigItem('allowed_retries', 1);
        if ($response->getStatus() >= 500 && $this->retries < $allowed_retries) {
            $this->retries++;
            return $this->authenticate($username, $password);
        }
        return $response;
    }

    protected function unhandledResponseToException(ResponseInterface $response, $body): Response
    {
        if ($response->getStatusCode() >= 500) {
            $e = new SystemException($body, null, $response->getStatusCode());
        } else {
            $e = new AuthenticationException($body, null, $response->getStatusCode());
        }
        $this->getLogger()
             ->error($e);
        return $this->getResponseModel()
                    ->fromException($e);
    }
}
