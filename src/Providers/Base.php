<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/24/18
 * Time: 2:56 PM
 */

namespace Smorken\Auth\Proxy\Common\Providers;

use Psr\Log\LoggerInterface;
use Smorken\Auth\Proxy\Common\Contracts\Enums\EndpointTypes;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Models\User;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Loggers\NullLogger;
use Smorken\Auth\Proxy\Common\Traits\Log;

abstract class Base implements Provider
{

    use Log;

    protected mixed $backend = null;

    protected array $config = [];

    protected array $default_config = [
        'endpoints' => [
            EndpointTypes::AUTHENTICATE => 'http://localhost/authenticate',
            EndpointTypes::SEARCH => 'http://localhost/search',
        ],
        'token' => null,
        'host' => null,
        'backend_options' => [],
    ];

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Models\Response|null
     */
    protected ?Response $response_model = null;

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Models\User|null
     */
    protected ?User $user_model = null;

    public function __construct(array $config)
    {
        $this->config = array_replace_recursive($this->default_config, $config);
        $this->debug = $this->getConfigItem('debug', false);
    }

    public function encode(?string $str): ?string
    {
        return $str;
    }

    public function getBackend(): mixed
    {
        return $this->backend;
    }

    public function getConfigItem(string $key, mixed $default = null): mixed
    {
        return $this->config[$key] ?? $default;
    }

    public function getEndpoint(string $type = EndpointTypes::AUTHENTICATE): ?string
    {
        $endpoints = $this->getConfigItem('endpoints', []);
        return $endpoints[$type] ?? null;
    }

    public function getResponseModel(): Response
    {
        if (!$this->response_model) {
            $this->response_model = new \Smorken\Auth\Proxy\Common\Models\Response();
        }
        return $this->response_model;
    }

    public function getUserModel(): User
    {
        if (!$this->user_model) {
            $this->user_model = new \Smorken\Auth\Proxy\Common\Models\User();
        }
        return $this->user_model;
    }

    protected function getLogger(): LoggerInterface
    {
        if (method_exists($this, 'getLog')) {
            return $this->getLog();
        }
        if (function_exists('app')) {
            return app(Log::class);
        }
        return new NullLogger();
    }
}
