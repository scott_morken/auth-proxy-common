<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 1:40 PM
 */

namespace Smorken\Auth\Proxy\Common\Models;

class User extends Base implements \Smorken\Auth\Proxy\Common\Contracts\Models\User
{
    protected array $attributes = [
        'id' => null,
        'username' => null,
        'first_name' => null,
        'last_name' => null,
        'email' => null,
        'data' => [],
    ];

    /**
     * @return bool
     */
    public function validate(): bool
    {
        return $this->id && $this->username;
    }

}
