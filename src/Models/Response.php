<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/21/18
 * Time: 1:52 PM
 */

namespace Smorken\Auth\Proxy\Common\Models;

class Response extends Base implements \Smorken\Auth\Proxy\Common\Contracts\Models\Response
{

    protected array $attributes = [
        'status' => null,
        'error' => false,
        'authenticated' => false,
        'message' => null,
    ];

    /**
     * @param  \Throwable  $e
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function fromException(\Throwable $e): static
    {
        $data = [
            'status' => 500,
            'error' => true,
            'authenticated' => false,
        ];
        if ($e instanceof \Smorken\Auth\Proxy\Common\Contracts\Exception) {
            $data = $this->handleLocalException($e, $data);
        } else {
            $data['status'] = $this->getDefaultStatus($e);
            $data['message'] = 'There was an error connecting to the authentication provider.';
        }
        return $this->newInstance($data);
    }

    public function fromUser(
        \Smorken\Auth\Proxy\Common\Contracts\Models\User $user,
        bool $authenticated = false
    ): static {
        $data = [
            'status' => 200,
            'error' => false,
            'authenticated' => $authenticated,
        ];
        $data = $this->addUser($user, $data);
        return $this->newInstance($data);
    }

    public function fromUsers(iterable $users): static
    {
        $data = [
            'status' => 200,
            'error' => false,
            'authenticated' => false,
            'users' => $users,
        ];
        return $this->newInstance($data);
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        $status = $this->attributes['status'] ?? null;
        if (!$status) {
            $status = 500;
            if ($this->isAuthenticated() || $this->hasUser() || $this->hasUsers()) {
                $status = 200;
            }
        }
        return $status;
    }

    public function hasUser(): bool
    {
        if (!$this->isError()) {
            $this->rehydrateUser($this);
            return (bool) $this->user;
        }
        return false;
    }

    public function hasUsers(): bool
    {
        if (!$this->isError()) {
            $this->rehydrateUsers($this);
            return $this->users && count($this->users) > 0;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        $this->rehydrateUser($this);
        return $this->authenticated === true && $this->hasUser() && $this->user->validate();
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error || $this->exception || $this->getStatus() !== 200;
    }

    /**
     * @param  array  $data
     * @return \Smorken\Auth\Proxy\Common\Contracts\Models\Response
     */
    public function rehydrate(array $data): static
    {
        $m = parent::rehydrate($data);
        $this->rehydrateUser($m);
        $this->rehydrateUsers($m);
        return $m;
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        $this->userToArray($data);
        $this->usersToArray($data);
        return $data;
    }

    protected function addUser(?\Smorken\Auth\Proxy\Common\Contracts\Models\User $user, array $data): array
    {
        if ($user && $user->validate()) {
            $data['user'] = $user->toArray();
        } else {
            $data['status'] = 401;
            $data['error'] = true;
            $data['authenticated'] = false;
            $data['message'] = 'Incomplete user data.';
        }
        return $data;
    }

    protected function getDefaultStatus(\Throwable $e): int
    {
        $code = $e->getCode();
        if ($code >= 400 && $code < 600) {
            return $code;
        }
        return 500;
    }

    protected function handleLocalException(\Smorken\Auth\Proxy\Common\Contracts\Exception $e, array $data): array
    {
        if ($e->getCode() !== 0) {
            $data['status'] = $e->getCode();
        } else {
            $data['status'] = 500;
        }
        $data['message'] = $e->display();
        return $data;
    }

    protected function rehydrateUser($m): void
    {
        $user = $m->getAttribute('user');
        if ($user && is_array($user)) {
            $m->setAttribute('user', new User($user));
        }
    }

    protected function rehydrateUsers($m): void
    {
        if ($m->users && count($m->users)) {
            $users = [];
            foreach ($m->users as $user) {
                $users[] = is_array($user) ? new User($user) : $user;
            }
            $m->users = $users;
        }
    }

    protected function userToArray(array &$data): void
    {
        if ($this->user) {
            $data['user'] = $this->user instanceof \Smorken\Auth\Proxy\Common\Contracts\Models\User
                ? $this->user->toArray() : $this->user;
        }
    }

    protected function usersToArray(array &$data): void
    {
        if ($this->users && count($this->users)) {
            $users = [];
            foreach ($this->users as $user) {
                if ($user instanceof \Smorken\Auth\Proxy\Common\Contracts\Models\User && $user->validate()) {
                    $users[] = $user->toArray();
                } elseif (is_array($user)) {
                    $users[] = $user;
                }
            }
            $data['users'] = $users;
        }
    }
}
