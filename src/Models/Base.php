<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/25/18
 * Time: 2:41 PM
 */

namespace Smorken\Auth\Proxy\Common\Models;

use Smorken\Auth\Proxy\Common\Contracts\Models\Model;

abstract class Base implements Model
{

    protected array $attributes = [];

    public function __construct(array $attributes = [])
    {
        foreach ($attributes as $k => $v) {
            $this->setAttribute($k, $v);
        }
    }

    public function __get(string $key): mixed
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, mixed $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function getAttribute(string $key): mixed
    {
        return $this->attributes[$key] ?? null;
    }

    public function newInstance(array $attributes = []): static
    {
        return $this->rehydrate($attributes);
    }

    /**
     * @param  array  $data
     * @return $this
     */
    public function rehydrate(array $data): static
    {
        return new static($data);
    }

    public function setAttribute(string $key, mixed $value): void
    {
        $this->attributes[$key] = $value;
    }

    public function toArray(): array
    {
        $attrs = [];
        foreach ($this->attributes as $k => $v) {
            $attrs[$k] = $this->getAttribute($k);
        }
        return $attrs;
    }

    public function toJson(): string
    {
        $attrs = $this->toArray();
        return json_encode($attrs);
    }
}
